export class StorageUser {
    static user = {
        token: '',
        user: {},
        alunos: [],
        client: '',
        menu: [],
        periodoLetivo: '',
        codigoescola: '',
        login: '',
        password: '',
        idDevice: ''
    }
    static aluno = {
        nome: '',
        matricula: '',
        dataNascimento: '',
        cod: '',
        foto: '',
        unidade: {
            nome: '',
            cnpj: '',
            email: '',
            cod: ''
        },
        turmas: []
    }
    static setUser (ss) {
        this.user = ss
        return "ok"
    }
    static setAluno (al) {
        this.aluno = al
        return "ok"
    }
}