import PushNotification from 'react-native-push-notification'
import firebase from 'react-native-firebase';
import {storage} from './storage';
import { isNil } from 'lodash'

export default class PushService {
  static init() {
    PushService.onNotification = (notification) => {
      if (!isNil(notification.title) && notification.title !== '' && notification.data) {
        PushNotification.localNotification({
          title: notification.title,
          message: notification.data.message
        })
      }
    }
    PushService.onRegistration = null
    PushService.tab = null
  }
  static setCallbacks(onRegistration, onNotification) {
    PushService.onRegistration = onRegistration
    PushService.onNotification = onNotification
  }
  static saveNotification(type, notification) {
    storage.remove({
      key: 'plugEscolaAolNotification'
    }).then(ret => {
      if (type === 'insert') {
        storage.save({
          key: 'plugEscolaAolNotification',
          data: JSON.stringify(notification)
        });
      }
    }).catch((error) => {
    })
  }
  static saveDevice(device) {
    // storage.remove({
    //   key: 'devicePushNotification'
    // }).then(ret => {
      // console.log(device)
      storage.save({
        key: 'devicePushNotification',
        data: device
      }).then((ret) => {

      }).catch((erro) => {
        console.log(erro)
      });
    // }).catch((erro) => {
    //   console.log('ERRO ao APAGAR o token GCM -------------------------------')
    //   console.log(erro)
    //   console.log('ERRO ao APAGAR o token GCM -------------------------------')
    // })
  }
  static configure() {
    PushNotification.configure({
      onRegister: function(token) {
        if (PushService.onRegistration) {
          PushService.onRegistration(token)
        }
        PushService.saveDevice(token)
      },
      onNotification: function(notification) {
        // notification.finish(PushNotificationIOS.FetchResult.NoData);
        if (PushService.onNotification) {
          PushService.saveNotification('insert', notification)
          PushService.onNotification(notification)
        }
      },
      //novo sender by matheus
      senderID: "463804130018",
      //antigo sender firebase
      // senderID: "63477722176",
      permissions: {
          alert: true,
          badge: true,
          sound: true
      },
      popInitialNotification: true,
      requestPermissions: true,
    })
  }

  static firebase() {
    const messaging = firebase.messaging();

    messaging.hasPermission()
      .then((enable) => {
        if(enable) {
          messaging.getToken()
            .then(token => { 
              PushService.saveDevice(token);
            })
            .catch(error => {
              console.log(error);
            })
        }
      })
      .catch(error => {
        console.log(error);
       });

    firebase.notifications().onNotification((notification) => {
      const { title, body} = notification;
      PushNotification.localNotification({
        title: title,
        message: body,
      });
    });
  }
}

export function getNotification() {
  PushNotification.localNotification({
    title: "My Notification Title",
    message: "My Notification Message",
  });
}

PushService.init()