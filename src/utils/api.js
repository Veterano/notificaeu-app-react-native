// F1 API v6
const F1 = 'https://apiplug.sger.com.br/v1/aol';
const F2 = 'https://apiplug.sger.com.br';

let api = {
  getLogin(data) {
    return fetch(F1+'/login', {  
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    }).then((res) => 
      res.json()
    )
  },
}

module.exports = api
