import { Dimensions,AsyncStorage } from 'react-native';
import Storage from 'react-native-storage'; 

const storage = new Storage({
  size: 1000,
  storageBackend: AsyncStorage,
  defaultExpires: 31536000000,
  enableCache: true,
  sync : {
  }
})


export {storage};