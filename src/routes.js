import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SignIn from './pages/SignIn/index';
import Main from './pages/main/index';

const Routes = createStackNavigator({
  SignIn,
  Main,
});

const RoutesCtn = createAppContainer(Routes);

export default RoutesCtn;