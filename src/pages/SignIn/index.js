import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { StatusBar } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation-stack';

//import api from '../../services/api';

import {
  Container,
  Logo,
  Input,
  ErrorMessage,
  Button,
  ButtonText,
} from './styles';

export default class SignIn extends Component {
  static navigationOptions = {
    header: null,
  };
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      dispatch: PropTypes.func,
    }).isRequired,
  };

  state = {
    idescola: '778899A',
    email: 'COD5566',
    password: '123456',
    error: '',
  };

  handleIdEscolaChange = (idescola) => {
    this.setState({ idescola });
  };
  handleEmailChange = (email) => {
    this.setState({ email });
  };

  handlePasswordChange = (password) => {
    this.setState({ password });
  };


  handleSignInPress = async () => {
    if (this.state.escolaid.length === 0 || this.state.email.length === 0 || this.state.password.length === 0) {
      this.setState({ error: 'Preencha usuário e senha para continuar!' }, () => false);
      console.log("validou")
    } else {
      try {
        const response = await api.post('', {
          idescola: this.state.idescola,
          email: this.state.email,
          password: this.state.password,
        });
        console.log('enviou')

        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Main', params: { token: response.data.token } }),
          ],
        });
        console.log('logou')
        this.props.navigation.dispatch(resetAction);
      } catch (_err) {
        this.setState({ error: 'Houve um problema com o login, verifique suas credenciais!' });
      }
    }
  };


  render() {
    return (
      <Container>
        <StatusBar hidden />
        <Logo source={require('../../images/teste.png')} resizeMode="contain" />
        <Input
          placeholder="Escola"
          value={this.state.idescola}
          onChangeText={this.handleIdEscolaChange}
          autoCapitalize="none"
          autoCorrect={false}
        />
        <Input
          placeholder="Usuario"
          value={this.state.email}
          onChangeText={this.handleEmailChange}
          autoCapitalize="none"
          autoCorrect={false}
        />
        <Input
          placeholder="Senha"
          value={this.state.password}
          onChangeText={this.handlePasswordChange}
          autoCapitalize="none"
          autoCorrect={false}
          secureTextEntry
        />
        {this.state.error.length !== 0 && <ErrorMessage>{this.state.error}</ErrorMessage>}
        <Button onPress={this.handleSignInPress}>
          <ButtonText>Entrar</ButtonText>
        </Button>
        <Button><ButtonText>Nao sei meu codigo!</ButtonText></Button>
      </Container>
    );
  }
}